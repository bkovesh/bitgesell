const bip39 = require('bip39');
const bip32 = require('bip32');
const axios = require('axios');
const bitcoin = require('bitcoinjs-lib');
const BigNumber = require('bignumber.js');
// const jsbgl = require('../libs/jsbgl2/jsbgl');
// const Address = require('../libs/jsbgl2/classes/address');
// const Transaction = require('../libs/jsbgl2/classes/transaction');
// const Wallet = require('../libs/jsbgl2/classes/wallet');
// const {SIGHASH_ALL} = require('../libs/jsbgl2/constants');


export default class BTC {
  constructor(net='testnet') {
    this.net = net;
    // this.url = 'http://localhost:5003';
    // this.url = 'http://localhost:5003/proxy';
    this.url = 'http://localhost:5003/wallets';
    this.urlExplorer = 'http://localhost:5003/explorer';
    // this.url = 'http://borigaldi:borigaldi@127.0.0.1:18332';
    // this.url = 'http://bgl_user:12345678@161.35.123.34:8332';
    this.jsonrpc = ({method,params=[]}) => {
      return {
        "jsonrpc": "1.0",
        "id":"curltest",
        "method": method,
        "params": params,
      }
    }
  }


  // BGL
  async genAddress () {
    await window.jsbtc.asyncInit();
    const { Address } = window.jsbtc;
    let address = new Address();
    return address;
  }


  // BGL
  async privateKeyToMnemonic ({privateKey}) {
    await window.jsbtc.asyncInit();
    const { entropyToMnemonic, Address } = window.jsbtc;
    let result = entropyToMnemonic(privateKey);
    return result;
  }


  // BGL
  async mnemonicToPrivateKey ({mnemonic}) {
    await window.jsbtc.asyncInit();
    const { mnemonicToEntropy } = window.jsbtc;
    let result = mnemonicToEntropy(mnemonic);
    return result;
  }


  // BGL
  async privateKeyToAddress ({privateKey}) {
    await window.jsbtc.asyncInit();
    const { Address } = window.jsbtc;
    let address = new Address(privateKey);
    return address.address;
  }


  // BGL
  async mnemonicToAddress ({mnemonic}) {
    await window.jsbtc.asyncInit();
    const { Address, mnemonicToEntropy } = window.jsbtc;
    let privateKey = mnemonicToEntropy(mnemonic);
    let address = new Address(privateKey);
    return address.address;
  }


  // BGL
  async getBlockchainInfo () {
    let result = await axios.post(this.url,this.jsonrpc({
      method:'getblockchaininfo',
      params:[],
    }))
    return result.data;
  }


  // BGL
  async getBlockStats ({height}) {
    let result = await axios.post(this.url,this.jsonrpc({
      method:'getblockstats',
      params:[height],
    }))
    return result.data;
  }


  // BGL works
  async getBalance ({address}) {
    let result = await axios.get(`${this.urlExplorer}/balance/${address}`);
    return result.data;
  }


  // BGL works
  async getHistory ({address}) {
    let result = await axios.get(`${this.urlExplorer}/history/${address}`);
    return result.data;
  }


  // BGL
  async getRawTx ({txid,verbose}) {
    let result = await axios.post(this.url,this.jsonrpc({
      method:'getrawtransaction',
      params:[txid,verbose],
    }))
    return result.data;
  }


  // BGL works
  async sendRawTx ({rawTx}) {
    let result = await axios.post(this.url,this.jsonrpc({
      method:'sendrawtransaction',
      params:[rawTx,0],
    }))
    return result.data;
  }


  // amount should be in BGL
  async signRawTx ({mnemonic, addressTo, amount}) {
    await window.jsbtc.asyncInit();
    const { Transaction, Address, SIGHASH_ALL } = window.jsbtc;
    // let a = new Address(privateKey);
    // let addressFrom = a.address;
    const addressFrom = await this.mnemonicToAddress({mnemonic})
    const privateKey = await this.mnemonicToPrivateKey({mnemonic})
    console.log('signRawTx addressFrom',addressFrom)
    console.log('signRawTx privateKey',privateKey)
    let history = await this.getHistory({address:addressFrom});
    history = history.data;
    console.log('signRawTx history',history)
    history = history.filter((item) => item.direction==='in')
    history.sort((a,b) => b.time-a.time)
    const txId = history[0].txid;
    const vOut = history[0].n;
    let balance = history[0].value; //TODO
    console.log('signRawTx txId',txId,vOut)
    let tx = new Transaction({ lockTime: 32681, segwit: true });
    // let balance = await this.getBalance({address:addressFrom});
    // balance = balance.data
    balance = balance * 10e7
    console.log('signRawTx balance',balance)
    const fee = 0.1 * 10e7;
    amount = amount * 10e7
    const amountLeft = balance - fee - amount;
    console.log('signRawTx amountLeft',amountLeft)
    tx.addInput({ txId, vOut, address: addressFrom });
    tx.addOutput({ value: amountLeft, address: addressFrom });
    tx.addOutput({ value: amount, address: addressTo });
    tx.signInput(0, { privateKey, sigHashType: SIGHASH_ALL, value: balance});
    let rawTx = tx.serialize();
    return rawTx;
  }




  // ========================================




  toSatoshi (amount) {
    return BigNumber(amount).multipliedBy(100000000).toNumber();
  }



  async signRawTx2 ({privateKey, addressTo, amount}) {
    await window.jsbtc.asyncInit();
    const { Transaction, Address, SIGHASH_ALL } = window.jsbtc;
    let a = new Address(privateKey);
    let addressFrom = a.address;
    let history = this.getHistory({address:addressFrom});
    // const privateKey = "L3cYuifXdrRpDgo6oyLZoCmndun5WcMe7tf7z61YXBgFN6AZziHR";
    // const addressTo = 'bgl1q044q8wuww2flvmkhla06p653jrh447u46z2v9w';
    const txId = "c26297290c022f02f88386b175bd225dbfeb887bba5f5cba155fbe43c110afe0";
    const vOut = 0;
    let tx = new Transaction({ lockTime: 32600, segwit: true });
    const balance = this.getBalance({address:addressFrom});
    const fee = 0.1 * 10e7;
    const amountLeft = balance - fee - amount;
    tx.addInput({ txId, vOut, address: addressFrom });
    tx.addOutput({ value: amountLeft, address: addressFrom });
    tx.addOutput({ value: amount, address: addressTo });
    tx.signInput(0, { privateKey, sigHashType: SIGHASH_ALL, value: balance});
    let rawTx = tx.serialize();
    return rawTx;
  }




  async createRawTxTest (params) {
    let result = await axios.post(this.url,this.jsonrpc({
      method:'createrawtransaction',
      params,
    }))
    return result.data.result;
  }



  async signRawTxTest2 (params) {
    const blockchaininfo = await this.getBlockchainInfo();
    const block = blockchaininfo.result.blocks;
    console.log(block)
    // https://bgl.bitaps.com/78c5454deb539ca6712a548ecef6b84061d79318a3ff565ce9854f403c37cc59
    // let rtx = "0200000000010151ab0ce41d17c55597011e798f6e634704cfd22d0debf0c4e0ee9c2c9cbb5f8e0000000000ffffffff0118ddf50500000000160014e0a5424502ad701c79ed7cb53f7ceabe9fd3b1e30247304402207b07de070579ec1c481ec858f339ea02b8269f554e70130e6f94f0dfe84dc45402202bfab5d7877ddcf57c7afd5dead1d708940382231007c166f83c31247d71c97d01210384fcc40094f0bdf493483dd5db6c7985d10122ef599e59be7436b59ca50de574c37e0000";
    // const privateKey = "KyfrGSgiv9rSiXiXBhJnDyZz8DU8VJdKYcbMGPginJUhrx7VSvAY"
    // const txId = "8e5fbb9c2c9ceee0c4f0eb0d2dd2cf0447636e8f791e019755c5171de40cab51";
    // const vOut = 0;
    const privateKey = "L3cYuifXdrRpDgo6oyLZoCmndun5WcMe7tf7z61YXBgFN6AZziHR";
    const addressTo = 'bgl1q044q8wuww2flvmkhla06p653jrh447u46z2v9w';
    const txId = "c26297290c022f02f88386b175bd225dbfeb887bba5f5cba155fbe43c110afe0";
    const txId2 = "bc9b6c0b673a913828af380b5b0b93287d161bffc42e417f7774bcd39ca24a04";
    const vOut = 0;
    await window.jsbtc.asyncInit();
    console.log(window.jsbtc)
    const Transaction = window.jsbtc.Transaction;
    const Address = window.jsbtc.Address;
    const SIGHASH_ALL = window.jsbtc.SIGHASH_ALL;
    // console.log(SIGHASH_ALL);
    let a = new Address(privateKey);
    console.log('Address',a)
    let addressFrom = a.address;
    console.log('addressFrom',addressFrom)
    let tx = new Transaction({lockTime: block,segwit:true,fee:0.1 * 100000000}); // locktime - number of this or next block
    // const balance = 50.00285 * 100000000;
    // const fee = 0.0004 * 100000000;
    // const amount = 1 * 100000000;
    // const amountLeft = balance - fee - amount;
    tx.addInput({ txId, vOut, address: addressFrom });
    // tx.addInput({ txId2, vOut, address: addressFrom });
    tx.addOutput({ value: 49.995 * 100000000, address: addressTo });
    tx.signInput(0, { privateKey, sigHashType: SIGHASH_ALL, value: 50 * 100000000});
    let rawTx = tx.serialize();
    console.log('Transaction',tx)
    return rawTx;
  }




}