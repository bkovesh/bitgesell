const crypto = require('crypto');
const pbkdf2 = require('pbkdf2');


export default class Crypto {
  constructor(saltRounds) {
    //
  }


  async encrypt (pin,data) {
    const salt = crypto.randomBytes(16);
    // console.log('encrypt',salt)
    const iv = crypto.randomBytes(16);
    // console.log('encrypt',iv)
    const key = crypto.pbkdf2Sync(String(pin), salt, 100000, 256/8, 'sha256');
    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    // console.log('encrypt',cipher)
    cipher.write(data);
    cipher.end()
    // console.log({
    //   iv: iv.toString('base64'),
    //   salt: salt.toString('base64'),
    //   encrypted: encrypted.toString('base64'),
    //   concatenned: Buffer.concat([salt, iv, encrypted]).toString('base64')
    // });
    const encrypted = cipher.read();
    return Buffer.concat([salt, iv, encrypted]).toString('base64')
  }


  async decrypt (pin,hash) {
    // console.log('decrypt',hash)
    const encrypted = Buffer.from(hash, 'base64');
    // console.log('decrypt',encrypted)
    const salt_len = 16;
    const iv_len = 16;
    const salt = encrypted.slice(0, salt_len);
    const iv = encrypted.slice(0+salt_len, salt_len+iv_len);
    // console.log('decrypt',iv)
    const key = crypto.pbkdf2Sync(String(pin), salt, 100000, 256/8, 'sha256');
    const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    decipher.write(encrypted.slice(salt_len+iv_len));
    decipher.end();
    const decrypted = decipher.read();
    // console.log('decrypt',decrypted.toString());
    return decrypted;
  }


}