import React from 'react';
import {BrowserRouter as Router, Switch, Link, Route, Redirect} from 'react-router-dom';
import * as Components from './components';
import ImageLogo from './assets/images/logo.png';

const style = {
  h1: {
    textDecoration: 'none',
  },
  logo: {
    width: 100,
    height: 100,
    margin: 20,
  },
}

function App() {
  return (
  <Router>
    <div className="App">
      <div className="AppContainer">

        <Link
        to="/"
        style={style.h1}
        >
          <img
          src={ImageLogo}
          style={style.logo}
          alt="Bitgesell"
          />
        </Link>

        <Switch>
          <Route exact path="/">
            <Components.PageSign/>
          </Route>
          <Route exact path="/login">
            <Components.PageLogin/>
          </Route>
          <Route exact path="/reg">
            <Components.PageReg/>
          </Route>
          <Route exact path="/pin">
            <Components.PagePin/>
          </Route>
          <Route exact path="/mnemonic">
            <Components.PageEnterMnemonic/>
          </Route>
          <Route exact path="/from-file">
            <Components.PageSignFromFile/>
          </Route>
          <Route exact path="/wallet">
            {/*{isLogged ? <Redirect to="/login" /> : <Components.PageWallet/>}*/}
            <Components.PageWallet/>
          </Route>
        </Switch>

      </div>
    </div>
  </Router>
  );
}

export default App;
