import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import * as Actions from '../../actions';
import s from './style.module.sass';
const BTC = new Actions.BTC();
const Crypto = new Actions.Crypto();



function PageReg() {
  let { path, url } = useRouteMatch();
  const [mnemonic, setMnemonic] = useState();
  const [isCopied, setIsCopied] = useState(false);
  const [pin, setPin] = useState('');


  useEffect(()=>{
    getMnemonic()
    run()
  },[])


  const run = async () => {
    const address = await BTC.genAddress()
    console.log("genAddress",address)
    const privateKey = address.privateKey.hex;
    console.log("privateKey",privateKey)
    const mnemonic = await BTC.privateKeyToMnemonic({privateKey})
    console.log("privateKeyToMnemonic",mnemonic)
    const pkey = await BTC.mnemonicToPrivateKey({mnemonic})
    console.log("mnemonicToPrivateKey",pkey)
    // const rawTx = await BTC.signRawTxTest()
    // console.log("run",rawTx)
    // const result = await BTC.sendRawTx({hex:rawTx})
    // console.log("run",result)
    // const result = await BTC.getUnspent();
    // const result = await BTC.createRawTx({
    //   fromWif: 'cV4qTLNqxQ3rYsgbZSQAsYUCmsandJTk6ScK5QJQ9JoKZoRjkibk',
    //   fromAddress: 'tb1qccls4ytjg2p3wzw9mmfph8j989eg0etl0nhxte',
    //   toAddress: 'tb1qccls4ytjg2p3wzw9mmfph8j989eg0etl0nhxte',
    //   amount: 1000, // in satoshi
    //   fee: 0.00001, // in BTC
    // })
    // const result = await BTC.createRawTxOld({
    //   fromWif: 'cV4qTLNqxQ3rYsgbZSQAsYUCmsandJTk6ScK5QJQ9JoKZoRjkibk',
    //   fromAddress: 'tb1qccls4ytjg2p3wzw9mmfph8j989eg0etl0nhxte',
    //   toAddress: 'tb1qccls4ytjg2p3wzw9mmfph8j989eg0etl0nhxte',
    //   amount: 1000, // in satoshi
    //   fee: 0.00001, // in BTC
    // })
    // const result = await BTC.getRawTx({
    //   txid: 'c26297290c022f02f88386b175bd225dbfeb887bba5f5cba155fbe43c110afe0',
    //   verbose: true,
    // })
    // const result = await BTC.getUnspent({
    //   addresses: ['bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3'],
    // })
    // const rawTx = await BTC.createRawTxTest([[
    //   {
    //     txid:'c26297290c022f02f88386b175bd225dbfeb887bba5f5cba155fbe43c110afe0',
    //     vout:0,
    //   }
    // ],[
    //   {"bgl1qt2u3sls4sucqr3ymclf6pqga09f2x4cyklexrs": 1,},
    //   {"bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3": 48.50285,},
    // ]])
    // const result = await BTC.sendRawTx({
    //   hex:rawTx,
    // })
    // const result = await BTC.signRawTxTest()
    // console.log('run',result)
  }


  const getMnemonic = async () => {
    const address = await BTC.genAddress()
    const privateKey = address.privateKey.hex;
    const mnemonic = await BTC.privateKeyToMnemonic({privateKey})
    setMnemonic(mnemonic)
  }


  const enter = async () => {
    if (!pin) return;
    const encrypted = await Crypto.encrypt(pin,mnemonic)
    localStorage.setItem('pin',pin)
    localStorage.setItem('encryptedMnemonic',encrypted)
    // const decrypted = await Crypto.decrypt(pin,encrypted)
    console.log('enter',encrypted)
  }


  const handleSetPin = async (e) => {
    localStorage.setItem('pin',e.target.value)
    setPin(e.target.value)
  }


  return (
    <div className={s.container}>

      <div className={s.m30}>
        <div className={s.text}>
          Your seed phrase. Please, save it somewhere and keep in secret.
        </div>
        <div
        className={s.seed}
        onClick={() => {
          navigator.clipboard.writeText(mnemonic)
          setIsCopied(true)
          setTimeout(() => setIsCopied(false),1000)
        }}
        >
          {!isCopied ? mnemonic : 'Copied!'}
        </div>
      </div>

      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <input
        className={s.input}
        placeholder="_ _ _ _"
        type="password"
        value={pin}
        onChange={handleSetPin}
        />
      </div>

      <div>
        <Link
        className={s.link}
        to="wallet"
        >
          <div
          className={s.btnBig}
          onClick={enter}
          >
            Continue
          </div>
        </Link>
      </div>

    </div>
  );
}

export default PageReg;
