import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';

function PageReg() {
  let { path, url } = useRouteMatch();
  const [pin, setPin] = useState('');
  const [isCorrect, setIsCorrect] = useState(false);

  useEffect(()=>{
    const pinFromStorage = localStorage.getItem('pin')
    setIsCorrect(false)
    if (pinFromStorage===pin) setIsCorrect(true)
  },[pin])

  const handleSetPin = (e) =>  {
    setPin(e.target.value)
  }

  return (
    <div className={s.container}>


      <div>
        <div className={s.text}>
          Please, enter your PIN
        </div>

        <input
        className={s.input}
        placeholder="_ _ _ _"
        type="password"
        onChange={handleSetPin}
        />
      </div>


      {isCorrect &&
      <div>
        <Link
        className={s.link}
        to="wallet"
        >
          <div className={s.btnBig}>
            Continue
          </div>
        </Link>
      </div>
      }

    </div>
  );
}

export default PageReg;
