import React from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import * as Components from '../../components';
import s from './style.module.sass';

function PageSign() {
  let { path, url } = useRouteMatch();




  return (
    <div className={s.container}>
      <h2>
        Welcome
      </h2>


      <Link
      className={s.link}
      to="/pin"
      >
        <div className={s.btnBig}>
          Enter
        </div>
      </Link>

      <Link
      className={s.link}
      to="/mnemonic"
      >
        <div className={s.btnBig}>
          With mnemonic
        </div>
      </Link>

      <Link
      className={s.link}
      to="/from-file"
      >
        <div className={s.btnBig}>
          From file
        </div>
      </Link>

      <Link
      className={s.link}
      to="/reg"
      >
        <div className={s.btnBig}>
          New wallet
        </div>
      </Link>

    </div>
  );
}

export default PageSign;
