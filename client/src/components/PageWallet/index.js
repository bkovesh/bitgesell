import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import moment from 'moment';
import * as Components from '../../components';
import * as Actions from '../../actions';
import s from './style.module.sass';
import IconLink from '../../assets/icons/link.svg';
const BTC = new Actions.BTC('testnet');
const Crypto = new Actions.Crypto();


const style = {
  table: {
    borderCollapse: 'collapse'
  },
  th: {
    borderBottom: '1px solid lightgrey',
    padding:10,
  },
  tdBold: {
    fontWeight:'bold',
    padding:10,
  },
  tx: {
    padding:10,
  },
  toAddress: {
    padding:5,
  },
  tac: {
    textAlign:'center',
  },
  link: {
    textDecoration:'none',
    color:'#fff',
  },
  hr: ({color='lightgrey',height=10}) => { return {
    width: '100%',
    height: 1,
    backgroundColor: color,
    margin: height/2,
  }},
}


function PageWallet() {
  let { path, url } = useRouteMatch();
  const [openMenu, setOpenMenu] = useState(false);
  const [isCopied, setIsCopied] = useState(false);
  const [mnemonic, setMnemonic] = useState();
  const [address, setAddress] = useState('');
  const [receiver, setReceiver] = useState('');
  const [amount, setAmount] = useState(0);
  const [balance, setBalance] = useState();
  const [history, setHistory] = useState();
  const [pin, setPin] = useState('');
  const [txId, setTxId] = useState('');
  history && history.sort((a,b) => b.time-a.time)


  useEffect(()=>{
    getSeedFromLocalStorage()
    run()
  },[])


  useEffect(()=>{
    mnemonic && getAddressFromMnemonic()
  },[mnemonic])


  useEffect(()=>{
    if (!address) return
    getBalance()
    getHistory()
  },[address])



  const run = async () => {
    const mnemonic = await BTC.privateKeyToMnemonic({
      privateKey: '00779d2e852d627595db1860086e8c59c35e5e0f11b2ca60bf209f1365c0d0f8'
    })
    console.log('privateKeyToMnemonic',mnemonic)
  }


  const getAddressFromMnemonic = async () => {
    const address = await BTC.mnemonicToAddress({mnemonic})
    console.log('getAddressFromMnemonic',mnemonic,address)
    setAddress(address)
  }


  const getBalance = async () => {
    const result = await BTC.getBalance({
      // address:'bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3',
      address,
    })
    console.log('getBalance',result)
    setBalance(result.data)
  }


  const getHistory = async () => {
    const result = await BTC.getHistory({
      // address:'bgl1qqgu9ct4guf9hqrntly7et20dym4nnytuc0tjq3',
      address,
    })
    console.log('getHistory',result)
    setHistory(result.data)
  }


  const genAddress = async () => {
    const result = await BTC.genAddress()
    console.log('genAddress',result)
    setReceiver(result)
  }



  const getSeedFromLocalStorage = async () => {
    const encryptedMnemonic = localStorage.getItem('encryptedMnemonic')
    const pin = localStorage.getItem('pin')
    console.log('encryptedMnemonic',encryptedMnemonic,pin)
    const decrypted = await Crypto.decrypt(pin,encryptedMnemonic)
    const mnemonic = decrypted.toString()
    console.log('getSeedFromLocalStorage',mnemonic)
    setMnemonic(mnemonic)
  }


  const send = async () => {
    const rawTx = await BTC.signRawTx({
      mnemonic,
      amount,
      addressTo: receiver,
    })
    const result = await BTC.sendRawTx({rawTx})
    setTxId(result.result)
    console.log('send',result)
  }


  const downloadMnemonic = () => {
    const element = document.createElement("a");
    const file = new Blob([mnemonic], {type: 'text/plain'});
    element.href = URL.createObjectURL(file);
    element.download = "Bitgesell.txt";
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  }


  return (
    <div className={s.container}>


      <div
      style={{
        position: 'fixed',
        top:0,right:0,
        padding: 20,
        cursor: 'pointer',
      }}
      onClick={() => setOpenMenu(true)}
      >
        <div
        >
          Menu
        </div>
      </div>


      { openMenu &&
      <div
      style={{
        position: 'fixed',
        top:0,right:0,
        width:200,
        backgroundColor:'#28272E',
        padding: 10,
        height:'100vh',
      }}
      >
        <div
        style={{
          cursor: 'pointer',
          padding:10,
          textAlign:'right',
        }}
        onClick={() => setOpenMenu(false)}
        >
          Close
        </div>
        <div
        style={{
          cursor: 'pointer',
          padding:10
        }}
        onClick={downloadMnemonic}
        >
          Backup
        </div>
      </div>
      }




      <div
      style={{cursor:'pointer'}}
      onClick={() => {
        navigator.clipboard.writeText(address)
        setIsCopied(true)
        setTimeout(() => setIsCopied(false),1000)
      }}
      >
        {!isCopied ? address : 'Copied!'}
      </div>



      <div className={s.balance}>
        { balance!==undefined ? String(balance).slice(0,10) :
        <span>Loading...</span>
        }
        {' '}
        <span className={s.balanceCurrency}>BGL</span>
      </div>

      <div style={style.hr({color:'transparent',height:30})}></div>


      <h2>
        Operations
      </h2>


      <div
      style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems:'center'
      }}
      >
        <input
        className={s.input}
        type="text"
        placeholder="Receiver"
        value={receiver}
        onChange={(e) => setReceiver(e.target.value)}
        />
      </div>
      <div
      style={{cursor:'pointer'}}
      onClick={genAddress}
      >
        Generate new address
      </div>
      <input
      className={s.input}
      type="text"
      placeholder="Amount"
      value={amount}
      onChange={(e) => setAmount(e.target.value)}
      />
      <div
      className={s.btnBig}
      onClick={send}
      >
        Send
      </div>

      <div style={style.hr({color:'transparent',height:10})}></div>

      <div>
        {`https://bgl.bitaps.com/${txId}`}
      </div>


      <div style={style.hr({color:'transparent',height:40})}></div>

      <h2>
        History
      </h2>

      <div>
        { history && history.map((tx,it) => {
          let {txid, hash,confirmations,time,value, direction} = tx;
          let addressFrom = tx.scriptPubKey.addresses[0]
          // let toAddress = out[0].addr ? out[0].addr.slice(0,15)+'...' : 'incoming tx';
          // let isIncoming = out[0].addr ? true : false;
          let isIncoming = direction==='in'
          let toAddress = false
          return (
          <div
          key={`tx-${it}`}
          className={s.tx}
          >
            <div className={isIncoming ? s.txValueIn : s.txValueOut}>
              <span className={s.txValueCount}>
                {isIncoming ? '+' : '-'}{' '}
                {value}
              </span>
              {' '}BGL
            </div>

            <div style={{marginRight:20,fontWeight:500}}>
              {isIncoming ?
              <span style={{color: '#585858', fontWeight: 500}}>from:</span> :
              <span style={{color: '#585858', fontWeight: 500}}>to:</span>
              }
              {' '}{addressFrom}
            </div>

            <div style={{marginRight:20,color:'#585858',fontWeight:500}}>
              {moment.unix(time).fromNow()}
            </div>

            { confirmations &&
            <div>
              Confirmations:{' '}{confirmations}
            </div>
            }

            {/*<div>*/}
            {/*  {moment.unix(time).fromNow()}*/}
            {/*</div>*/}
            {/*<div style={{...style.tdBold,...style.tac}}>*/}
            {/*  {isIncoming ? '' : '-'}{result}*/}
            {/*</div>*/}
            {/*<div style={style.tac}>*/}
            {/*  {toAddress}*/}
            {/*</div>*/}
            <div style={{...style.tac,...{}}}>
              <a
              style={style.link}
              href={`https://bgl.bitaps.com/${txid}`}
              target="_blank"
              rel="noopener noreferrer"
              >
                <img
                style={{width:20,height:20}}
                src={IconLink}
                alt="Show"
                />
              </a>
            </div>
          </div>
          )
        })}
        { history && history.length===0 && 'You have no transactions'}
      </div>

      <div style={style.hr({color:'transparent',height:80})}></div>

    </div>
  );
}

export default PageWallet;
