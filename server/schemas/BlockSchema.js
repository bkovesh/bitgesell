const mongoose = require('mongoose');


const BlockSchema = new mongoose.Schema({
  "hash": String,
  "confirmations": Number,
  "strippedsize": Number,
  "size": Number,
  "weight": Number,
  "height": Number,
  "version": Number,
  "versionHex": String,
  "merkleroot": String,
  "tx": [String],
  "time": Number,
  "mediantime": Number,
  "nonce": Number,
  "bits": String,
  "difficulty": Number,
  "chainwork": String,
  "nTx": Number,
  "nextblockhash": String
})


module.exports = mongoose.model('Block', BlockSchema);

