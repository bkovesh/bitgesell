const mongoose = require('mongoose');


const TxSchema = new mongoose.Schema({
  "txid": String,
  "hash": String,
  "version": Number,
  "size": Number,
  "vsize": Number,
  "weight": Number,
  "locktime": Number,
  "vin": [],
  "vout": []
})


module.exports = mongoose.model('Tx', TxSchema);

