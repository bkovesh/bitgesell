var express = require('express');
var axios = require('axios');
var router = express.Router();
require('dotenv').config()
// let url = 'http://seed.satoshithefirst.com:18443'
// let url2 = 'http://seed.vitalikthesecond.com:8332'
// let url = 'http://185.132.132.166:18443/'
// let url = 'http://127.0.0.1:8455'
// let url = 'http://borigaldi:borigaldi@127.0.0.1:8455'
// let url = 'http://borigaldi:borigaldi@127.0.0.1:18332'
// let url = 'http://borigaldi:borigaldi@127.0.0.1:8332'
// let url = 'http://bgl_user:12345678@161.35.123.34:8332'
// let url = 'http://borigaldi:borigaldi@188.134.7.138:8455'
let url = process.env.BGL_URL;

router.post('/', async (req, res, next) => {
  try {
    // console.log(req.body)
    let result = await axios.post(url, req.body)
    return res.status(200).send(result.data);
  } catch (e) {
    console.error(e.response.data.error);
    return res.status(500).send(e);
  }
});



module.exports = router;
