var express = require('express');
var axios = require('axios');
var router = express.Router();
require('dotenv').config()
const Block = require('../schemas/BlockSchema')
const Tx = require('../schemas/TxSchema')
const URL = process.env.BGL_URL;


const jsonRpc = ({method,params}) => {
  return {
    "jsonrpc": "1.0",
    "id":"curltest",
    "method": method,
    "params": params,
  }
}


const updateExplorerManually = async () => {
  // const url = 'http://borigaldi:borigaldi@127.0.0.1:8455';
  const blocksInDb = await Block.find({});
  let lastBlockNumber = blocksInDb.length;
  if (lastBlockNumber===0) lastBlockNumber = 1;
  lastBlockNumber = 32000
  let blockchaininfo = await axios.post(URL, jsonRpc({
    method:'getblockchaininfo',
    params: [],
  }))
  let blockCount = blockchaininfo.data.result.blocks;
  console.log('blockCount',blockCount)
  lastBlockNumber = blockCount - 100
  if (lastBlockNumber >= blockCount) {
    console.log('Already up to date',lastBlockNumber)
    return 'Already up to date'
  }
  let blocks = []
  let allTxs = []
  for (let i = lastBlockNumber; i < lastBlockNumber+5; i++) {
    let blockhash = await axios.post(URL, jsonRpc({
      method:'getblockhash',
      params: [i],
    }))
    let block = await axios.post(URL, jsonRpc({
      method:'getblock',
      params: [blockhash.data.result],
    }))
    blocks.push(block.data.result)
    let txs = block.data.result.tx;
    for (let j = 0; j < txs.length; j++) {
      let txRaw = await axios.post(URL, jsonRpc({
        method:'getrawtransaction',
        params: [txs[j]],
      }))
      let tx = await axios.post(URL, jsonRpc({
        method:'decoderawtransaction',
        params: [txRaw.data.result],
      }))
      allTxs.push(tx.data.result)
    }
  }
  const resultBlock = await Block.insertMany(blocks)
  const result = await Tx.insertMany(allTxs)
  return result
}


// get last block number
// check if new block exists
// add new block and new txs to db
const updateExplorer = async () => {
  // const url = 'http://borigaldi:borigaldi@127.0.0.1:8455';
  // console.log(req.body)
  let blockchaininfo = await axios.post(URL, jsonRpc({
    method:'getblockchaininfo',
    params: [],
  }))
  let blockCount = blockchaininfo.data.result.blocks;
  let blocksInDb = []
  blocksInDb = await Block.find({});
  console.log('blocksInDb',blocksInDb[blocksInDb.length-1])
  let lastBlockNumber = blocksInDb[blocksInDb.length-1] && blocksInDb[blocksInDb.length-1].height;
  // lastBlockNumber = 32000;
  if (!lastBlockNumber) lastBlockNumber = blockCount - 500
  console.log('lastBlockNumber',blockCount)
  if (lastBlockNumber >= blockCount) {
    console.log('Already up to date',lastBlockNumber)
    return 'Already up to date'
  }
  let blocks = []
  let allTxs = []
  for (let i = lastBlockNumber; i < blockCount; i++) {
    let blockhash = await axios.post(URL, jsonRpc({
      method:'getblockhash',
      params: [i],
    }))
    let block = await axios.post(URL, jsonRpc({
      method:'getblock',
      params: [blockhash.data.result],
    }))
    blocks.push(block.data.result)
    let txs = block.data.result.tx;
    for (let j = 0; j < txs.length; j++) {
      let txRaw = await axios.post(URL, jsonRpc({
        method:'getrawtransaction',
        params: [txs[j]],
      }))
      let tx = await axios.post(URL, jsonRpc({
        method:'decoderawtransaction',
        params: [txRaw.data.result],
      }))
      allTxs.push(tx.data.result)
    }
  }
  const resultBlock = await Block.insertMany(blocks)
  const result = await Tx.insertMany(allTxs)
  return result
}

setInterval(() => {
  console.log('run')
},30*1000)

// ==========================================


router.get('/update-explorer-manually', async (req, res, next) => {
  try {
    const result = await updateExplorerManually();
    return res.status(200).send({data:'Done'});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});



router.get('/update-explorer', async (req, res, next) => {
  try {
    const result = await updateExplorer();
    return res.status(200).send({data:'Done'});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});



router.get('/txs-of-address/:address', async (req, res, next) => {
  try {
    const {address} = req.params
    const txs = await Tx.find({"vout.scriptPubKey.addresses":address})
    return res.status(200).send({data:txs});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});



router.get('/tx/:txid', async (req, res, next) => {
  try {
    const {txid} = req.params
    const txs = await Tx.find({txid})
    return res.status(200).send({data:txs});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});


router.get('/tx-by-vin/:txid', async (req, res, next) => {
  try {
    const {txid} = req.params
    const txs = await Tx.find({"vin.txid":txid})
    return res.status(200).send({data:txs});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});



router.get('/balance/:address', async (req, res, next) => {
  try {
    const {address} = req.params
    const txsOut = await Tx.find({"vout.scriptPubKey.addresses":address})
    let balance = 0;
    for (let i = 0; i < txsOut.length; i++) {
      let txOut = txsOut[i];
      const vOut = txOut.vout
      const n = txOut.vout[0].n
      const value = txOut.vout[0].value
      const txsIn = await Tx.find({"vin.txid":txOut.txid})
      for (let j = 0; j < txsIn.length; j++) {
        let txIn = txsIn[j];
        // console.log(value,n===txIn.vin[0].vout)
        if (n===txIn.vin[0].vout) balance -= value
      }
      balance += txOut.vout[0].value
    }
    return res.status(200).send({data:balance});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});


router.get('/history/:address', async (req, res, next) => {
  try {
    const {address} = req.params
    const history = []
    const txsOut = await Tx.find({"vout.scriptPubKey.addresses":address})
    let balance = 0;
    for (let i = 0; i < txsOut.length; i++) {
      let txOut = txsOut[i];
      const vOut = txOut.vout
      const n = txOut.vout[0].n
      const value = txOut.vout[0].value
      const txsIn = await Tx.find({"vin.txid":txOut.txid})
      for (let j = 0; j < txsIn.length; j++) {
        let txIn = txsIn[j];
        // console.log(value,n===txIn.vin[0].vout)
        txIn.vout[0].txid = txIn.txid
        let time = await axios.post(URL, jsonRpc({
          method:'getblockstats',
          params: [txIn.locktime],
        }))
        txIn.vout[0].locktime = txIn.locktime
        txIn.vout[0].confirmations = txIn.confirmations
        txIn.vout[0].time = time.data.result.time
        txIn.vout[0].direction = 'out'
        if (n===txIn.vin[0].vout) history.push(txIn.vout[0])
      }
      txOut.vout[0].txid = txOut.txid
      let time = await axios.post(URL, jsonRpc({
        method:'getblockstats',
        params: [txOut.locktime],
      }))
      txOut.vout[0].locktime = txOut.locktime
      txOut.vout[0].confirmations = txOut.confirmations
      txOut.vout[0].time = time.data.result.time
      txOut.vout[0].direction = 'in'
      history.push(txOut.vout[0])
    }
    return res.status(200).send({data:history});
  } catch (e) {
    console.error(e.message);
    return res.status(500).send(e);
  }
});


// ==========================================

module.exports = router;
