var createError = require('http-errors');
var http = require('http');
var express = require('express');
var cors = require('cors');
var mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bitcoin = require('bitcoin-rpc-api');
var { createProxyMiddleware } = require('http-proxy-middleware');
require('dotenv').config()


var indexRouter = require('./routes/index');
var walletsRouter = require('./routes/wallets');
var explorerRouter = require('./routes/explorer');



var node = {
  protocol: 'http',
  host: '127.0.0.1',
  // host: '192.168.1.64',
  port: 8455, // bitgesell
  // port: 8333, //mainnet
  // port: 18332, //testnet
  // port: 18333, //regtest
  user: 'borigaldi',
  pass: 'borigaldi'
};


// this.url = 'http://bgl_user:12345678@161.35.123.34:8332';
// var node = {
//   protocol: 'http',
//   host: '161.35.123.34',
//   // host: '192.168.1.64',
//   // port: 8455, // bitgesell
//   // port: 8333, //mainnet
//   port: 8332, //testnet
//   // port: 18333, //regtest
//   user: 'bgl_user',
//   pass: '12345678'
// };

bitcoin.setup(node);

// ====================================

var app = express();

// view engine setup
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use('/', indexRouter);
app.use('/btc', bitcoin.api);
app.use('/wallets', walletsRouter);
app.use('/explorer', explorerRouter);


// ====================================

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

// ====================================




const dbOpts = {
  autoIndex: false,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

mongoose.connect(process.env.DB_URL, dbOpts, (err) => {
  if(err) return console.log(err);
  var PORT = 5003;
  app.set('port', PORT);
  var server = http.createServer(app);
  server.listen(PORT);
  server.on('error', (e) => console.error(e));
  server.on('listening', (m) => console.log(`Server is listening on http://localhost:${PORT}`));
});



module.exports = app;
