const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer({
  target:'http://borigaldi:borigaldi@127.0.0.1:18332'
}).listen(5003);

proxy.on('proxyReq', function(proxyReq, req, res, options) {
  res.setHeader('X-Special-Proxy-Header', 'foobar');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-AUTHENTICATION, X-IP, X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization');
});